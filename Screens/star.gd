extends Sprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _physics_process(delta):
 get_input()
	
func get_input():
	var offset = Vector2(0, 0)
	
	var accel = Input.get_accelerometer()

	if accel.x > 0:
		offset.x = lerp(get_offset().x + accel.x, 100, 0.01)

	elif accel.x < 0:
		offset.x = lerp(get_offset().x + accel.x, -100, 0.01)
		
	if accel.y < 0:
		offset.y = lerp(get_offset().y + accel.y, -100, 0.01)

	elif accel.y > 0:
		offset.y = lerp(get_offset().y + accel.y, 100, 0.01)

	var vWidth = get_viewport_rect().size.x
	var vHeight = get_viewport_rect().size.y
	
	var mPos = get_viewport().get_mouse_position()

#	if mPos.x > vWidth/2:
#		offset.x = lerp(get_offset().x, (mPos.x/vWidth * vWidth/2)/4, 0.01)
#
#	elif mPos.x < vWidth/2:
#		offset.x = lerp(get_offset().x, -((mPos.x/vWidth * vWidth)/4), 0.01)
#		
#	if mPos.y > vHeight/2:
#		offset.y = lerp(get_offset().y, (mPos.y/vHeight * vHeight/2)/4, 0.01)
#
#	elif mPos.y < vHeight/2:
#		offset.y = lerp(get_offset().y, -((mPos.y/vHeight * vHeight/2)/4), 0.01)
	
	set_offset(offset)

func get_input1():
	var offset = Vector2(0, 0)
	if Input.is_action_pressed("ui_left"):
		offset.x = lerp(get_offset().x, -100, 0.05)
		set_offset(offset)

	elif Input.is_action_pressed("ui_right"):
		offset.x = lerp(get_offset().x, 100, 0.05)
		set_offset(offset)

